﻿using SerialPortHelper.Command;
using SerialPortHelper.Model;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Windows;

namespace SerialPortHelper.Port
{
    class ComManager
    {
        public static readonly ComManager instance = new ComManager();

        private SerialPort SerialPort { get; set; } = null;

        public string[] GetPorts()
        {
            return SerialPort.GetPortNames();
        }
        
        public bool Open()
        {
            if (SerialPort != null && SerialPort.IsOpen)
                return Close();
            try
            {
                //new Serial Port
                SerialPort = new SerialPort();

                //setup baudrate
                SerialPort.BaudRate = MainModel.instance.GetBaudRate();

                //setup data bit
                SerialPort.DataBits = MainModel.instance.GetDataBit();

                //setup stopbit
                switch (MainModel.instance.GetStopBit())
                {
                    case 1:
                        SerialPort.StopBits = System.IO.Ports.StopBits.One;
                        break;
                    case 1.5:
                        SerialPort.StopBits = System.IO.Ports.StopBits.OnePointFive;
                        break;
                    case 2:
                        SerialPort.StopBits = System.IO.Ports.StopBits.Two;
                        break;
                }

                //setup parity
                switch (MainModel.instance.GetParity())
                {
                    case "None":
                        SerialPort.Parity = System.IO.Ports.Parity.None;
                        break;
                    case "Odd":
                        SerialPort.Parity = System.IO.Ports.Parity.Odd;
                        break;
                    case "Even":
                        SerialPort.Parity = System.IO.Ports.Parity.Even;
                        break;
                }

                //setup port name
                SerialPort.PortName = MainModel.instance.GetPort();

                //setup read
                SerialPort.DataReceived += Read;

                //Open port
                SerialPort.Open();

                return SerialPort.IsOpen;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }

        public bool Close()
        {
            if (SerialPort.IsOpen)
                SerialPort.Close();
            return SerialPort.IsOpen;
        }

        public void Send()
        {
            if(SerialPort==null||!SerialPort.IsOpen)
            {
                ImplementCommand.instance.AppendMessage("[Error] Port open fail.\n");
                return;
            }
            byte[] bytes = CRC.Hex2Byte(MainModel.instance.sendData);
            SerialPort.Write(bytes,0,bytes.Length);
            ImplementCommand.instance.AppendMessage("-> " + MainModel.instance.sendData + "\n");
        }

        public void Read(object sender,SerialDataReceivedEventArgs e)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                if(SerialPort.BytesToRead==0)
                    sb.Append("<- null\n");
                else
                {
                    byte[] buf = new byte[SerialPort.BytesToRead];
                    SerialPort.Read(buf, 0, SerialPort.BytesToRead);
                    sb.Append("<- ");
                    foreach (byte b in buf)
                        sb.Append(b.ToString("X2" + ""));
                }
                ImplementCommand.instance.AppendMessage(sb.ToString()+"\n");
                sb.Clear();
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void SetEnableRts(bool enableRts)
        {
            //setup rts
            SerialPort.RtsEnable = enableRts;
            while((SerialPort.RtsEnable && !SerialPort.CtsHolding) || (!SerialPort.RtsEnable && SerialPort.CtsHolding))
            {
                ImplementCommand.instance.AppendMessage(string.Format("[Status] Waiting for RTS line to go {0}\n",enableRts?"high":"low"));
                Thread.Sleep(100);
            }
            ImplementCommand.instance.AppendMessage(string.Format("[Status] RTS line go to {0} success\n", enableRts ? "high" : "low"));
        }
    }
}
