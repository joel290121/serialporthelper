﻿using System;
using System.Windows.Input;

namespace SerialPortHelper.Command
{
    class DelegateCommand : ICommand
    {
        public Action<object> executeAction { get; set; }
        public Func<object, bool> canExecute { get; set; }

        public DelegateCommand(Action<object> executeAction, Func<object, bool> canExecute)
        {
            this.executeAction = executeAction;
            this.canExecute = canExecute;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return canExecute == null || canExecute.Invoke(parameter);
        }

        public void Execute(object parameter)
        {
            executeAction(parameter);
        }
    }
}
