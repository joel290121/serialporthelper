﻿using SerialPortHelper.Model;
using SerialPortHelper.Port;
using SerialPortHelper.ViewModel;

namespace SerialPortHelper.Command
{
    class ImplementCommand
    {
        private ImplementCommand() { }
        public static readonly ImplementCommand instance = new ImplementCommand();

        MainViewModel mainViewModel;
        public void SetMainViewModel(MainViewModel mainViewModel)=> this.mainViewModel = mainViewModel;

        //Open Com
        public bool ToOpen()
        {
            return ComManager.instance.Open();
        }

        //Send Data
        public void ToSend()=>ComManager.instance.Send();

        //Enable Rts
        public void EnableRts() => ComManager.instance.SetEnableRts(MainModel.instance.enableRts);

        public void AppendMessage(string message)=> mainViewModel.AppendMessage(message);
    }
}
