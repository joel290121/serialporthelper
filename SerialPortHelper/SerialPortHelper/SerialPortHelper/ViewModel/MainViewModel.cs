﻿using SerialPortHelper.Command;
using SerialPortHelper.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace SerialPortHelper.ViewModel
{
    class MainViewModel:ViewModelBase
    {
        /**
         * Port
         **/
        //sync to view
        public ObservableCollection<string> Ports { get; set; } = new ObservableCollection<string>(MainModel.instance.ports);
        //sync from view
        public string Port 
        {
            get => MainModel.instance.GetPort();
            set => MainModel.instance.SetPort(value);
        }

        /**
         * BaudRate
         **/
        //sync to view
        public ObservableCollection<int> BaudRates { get; set; } =new ObservableCollection<int>(MainModel.instance.baudRates);
        //sync from view
        public int BaudRate 
        {
            get => MainModel.instance.GetBaudRate();
            set => MainModel.instance.SetBaudRate(value);
        }

        /**
         * Data Bit
         **/
        //sync to view
        public ObservableCollection<int> DataBits { get; set; } = new ObservableCollection<int>(MainModel.instance.dataBits);
        //sync from view
        public int DataBit
        {
            get => MainModel.instance.GetDataBit();
            set => MainModel.instance.SetDataBit(value);
        }

        /**
         * Parity
         **/
        //sync to view
        public ObservableCollection<string> Paritys { get; set; } = new ObservableCollection<string>(MainModel.instance.paritys);
        //sync from view
        public string Parity
        {
            get => MainModel.instance.GetParity();
            set => MainModel.instance.SetParity(value);
        }

        /**
         * Stop bit
         **/
        //sync to view
        public ObservableCollection<double> StopBits { get; set; } = new ObservableCollection<double>(MainModel.instance.stopBits);
        //sync from view
        public double StopBit
        {
            get => MainModel.instance.GetStopBit();
            set => MainModel.instance.SetStopBit(value);
        }

        public bool EnableRts
        {
            get => MainModel.instance.GetEnableRts();
            set
            {
                MainModel.instance.SetEnableRts(value);
                ToEnableRts.Execute(null);
            }
        }

        /**
         * Enable Select
         **/
        public bool IsOpen
        {
            get=> MainModel.instance.isOpen;
            set => MainModel.instance.isOpen = value;
        }

        /**
         * data for send
         **/
        public string SendData
        {
            get=>MainModel.instance.sendData;
            set=>MainModel.instance.sendData=value;
        }

        /**
         * Message
         **/
        public string Message { get; set; }
        
        /**
         * Function
         **/
        //Open Comm
        public DelegateCommand ToOpenCom { get; set; }
        private void ToOpen(object parameter)
        {
            if (EnableRts)
                EnableRts = false;
            IsOpen=ImplementCommand.instance.ToOpen();
            AppendMessage(string.Format("[{0}] {1}\n","Status", IsOpen ? "Connected" : "Disconnect"));
        }

        //Send Data
        public DelegateCommand ToSendData { get; set; }
        private void ToSend(object parameter)
        {
            if (SendData.Replace(" ", "").Length == 0)
                return;
            ImplementCommand.instance.ToSend();
        }

        //clear message
        public DelegateCommand ToClearMessage { get; set; }
        private void ToClear(object parameter)=> ClearMessage();

        //setup enable rts
        public DelegateCommand ToEnableRts { get; set; }
        private void ToEnableRTS(object parameter) => ImplementCommand.instance.EnableRts();

        //initial
        public MainViewModel()
        {   
            Message = "";
            ImplementCommand.instance.SetMainViewModel(this);
            ToOpenCom = new DelegateCommand(ToOpen,null);
            ToSendData = new DelegateCommand(ToSend,null);
            ToClearMessage = new DelegateCommand(ToClear, null);
            ToEnableRts = new DelegateCommand(ToEnableRTS, null);
        }

        public void AppendMessage(string message)=> Message += message;

        private void ClearMessage()=> Message = "";
    }
}
