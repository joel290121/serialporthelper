﻿using SerialPortHelper.Port;
using System;

namespace SerialPortHelper.Model
{
    class MainModel
    {
        private MainModel() { }
        public static readonly MainModel instance = new MainModel();

        //Port
        public string[] ports = ComManager.instance.GetPorts();
        public int portIndex=0;
        public void SetPort(String value) => portIndex = Array.IndexOf(ports, value);
        public string GetPort() => ports[portIndex];

        //BaudRate
        public int[] baudRates = new int[] { 300, 600, 1200, 2400, 9600, 14400, 19200, 38400, 57600, 115200 };
        public int baudRate=115200;
        public void SetBaudRate(int value) => baudRate=value;
        public int GetBaudRate() => baudRate;

        //DataBit
        public int[] dataBits = new int[] { 6,7,8 };
        public int dataBit=8;
        public void SetDataBit(int value) => dataBit=value;
        public int GetDataBit() => dataBit;

        //Parity
        public string[] paritys = new string[] { "None", "Odd", "Even" };
        public int parityIndex = 0;
        public void SetParity(String value) => parityIndex = Array.IndexOf(paritys, value);
        public string GetParity() => paritys[parityIndex];

        //StopBit
        public double[] stopBits = new double[] { 1, 1.5, 2 };
        public double stopBit = 1;
        public void SetStopBit(double value) => stopBit = value;
        public double GetStopBit() => stopBit;

        public bool enableRts = false;
        public void SetEnableRts(bool enableRts)=> this.enableRts = enableRts;
        public bool GetEnableRts()=> enableRts;

        // Connect/Disconnect
        public bool isOpen = false;

        //Send data
        public string sendData = "";
    }
}
