﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SerialPortHelper
{
    class CRC
    {
        public static byte[] Hex2Byte(string hex)
        {
            if (string.IsNullOrEmpty(hex))
                return null;
            var offset = hex.Length % 2;
            if (offset == 1)
                hex = "0" + hex;
 
            var list = new List<byte>();
            for(int index=0;index<hex.Length;index+=2)
            {
                var temp = hex.Substring(index, 2);
                byte bv;
                var success = byte.TryParse(temp,System.Globalization.NumberStyles.HexNumber,null,out bv);
                if (!success)
                    throw new ArgumentOutOfRangeException();
                list.Add(bv);
            }
            return list.ToArray();
        }

        private static char[] hexDigit = new char[16] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        public static string Byte2Hex(byte[] data)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in data)
            {
                int value = b & 0xFF;
                sb.Append(hexDigit[value >> 4]);
                sb.Append(hexDigit[value&0x0F]);
            }
            return sb.ToString();
        }
    }
}
